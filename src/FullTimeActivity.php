<?php

declare(strict_types=1);

namespace Activities;

final class FullTimeActivity implements ActivityInterface
{
    private ?string $trainer = null;

    private bool $isExternal = false;

    public function __construct(
        private string $topic,
        private string $location
    ) {}

    public function getTopic(): string
    {
        return $this->topic;
    }

    public function getTrainer(): ?string
    {
        return $this->trainer;
    }

    public function appointTrainer(string $trainer): void
    {
        $this->trainer = $trainer;
    }

    public function getLocation(): string
    {
        return $this->location;
    }
}
