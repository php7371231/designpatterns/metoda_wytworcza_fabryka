<?php

declare(strict_types=1);

namespace Activities;

interface ActivityInterface
{
    public function getTopic(): string;

    public function getTrainer(): ?string;

    public function appointTrainer(string $trainer): void;

    public function getLocation(): string;

}
