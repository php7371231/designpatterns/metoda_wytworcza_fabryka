<?php

declare(strict_types=1);

namespace Activities\Usage;

use Activities\ActivityInterface;

interface ActivityRepositoryInterface
{
    public function save(ActivityInterface $activity): void;
}
