<?php

declare(strict_types=1);

namespace Activities\Usage;

use Activities\ActivityFactoryInterface;

final class ActivityOrganizer
{
    public function __construct(
        private ActivityRepositoryInterface $repository
    ) {}

    public function organize(
        string $topic,
        string $trainer,
        string $location,
        ActivityFactoryInterface $factory
    ): void {
        $Activity = $factory->create($topic, $location);
        $Activity->appointTrainer($trainer);

        $this->repository->save($Activity);
    }
}
