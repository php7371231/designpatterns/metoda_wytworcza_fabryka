<?php

declare(strict_types=1);

namespace Activities;

interface ActivityFactoryInterface
{
    public function create(string $topic, string $location): ActivityInterface;
}
