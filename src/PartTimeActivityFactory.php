<?php

declare(strict_types=1);

namespace Activities;

final class PartTimeActivityFactory implements ActivityFactoryInterface
{
    public function create(string $topic, string $location): ActivityInterface
    {
        return new PartTimeActivity($topic, $location);
    }
}
