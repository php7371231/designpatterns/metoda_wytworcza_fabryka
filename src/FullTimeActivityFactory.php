<?php

declare(strict_types=1);

namespace Activities;

final class FullTimeActivityFactory implements ActivityFactoryInterface
{
    public function create(string $topic, string $location): ActivityInterface
    {
        return new FullTimeActivity($topic, $location);
    }
}
