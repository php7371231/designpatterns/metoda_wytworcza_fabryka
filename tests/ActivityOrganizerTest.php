<?php

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use Tests\Fake\ActivityRepository;
use Activities\FullTimeActivity;
use Activities\FullTimeActivityFactory;
use Activities\PartTimeActivity;
use Activities\PartTimeActivityFactory;
use Activities\Usage\ActivityOrganizer;

final class ActivityOrganizerTest extends TestCase
{
    public function testOrganize_givenValidPartTimeParameters_shouldOrganizePartTimeActivity(): void
    {
        $topic = 'Jazda na rowerze';
        $trainer = 'John Amstrong';
        $location = 'London';
        $repository = new ActivityRepository();
        $SUT = new ActivityOrganizer($repository);

        $SUT->organize($topic, $trainer, $location, new PartTimeActivityFactory());
        $result = $repository->findOneByTopic($topic);

        self::assertInstanceOf(PartTimeActivity::class, $result);
        self::assertSame($topic, $result->getTopic());
        self::assertSame($trainer, $result->getTrainer());
        self::assertSame($location, $result->getLocation());
    }

    public function testOrganize_givenValidFullTimeParameters_shouldOrganizeFullTimeActivity(): void
    {
        $topic = 'Amerykanski Soccer';
        $trainer = 'Fernando Santos';
        $location = 'Polska';
        $repository = new ActivityRepository();
        $SUT = new ActivityOrganizer($repository);

        $SUT->organize($topic, $trainer, $location, new FullTimeActivityFactory());
        $result = $repository->findOneByTopic($topic);

        self::assertInstanceOf(FullTimeActivity::class, $result);
        self::assertSame($topic, $result->getTopic());
        self::assertSame($trainer, $result->getTrainer());
        self::assertSame($location, $result->getLocation());
    }
}
