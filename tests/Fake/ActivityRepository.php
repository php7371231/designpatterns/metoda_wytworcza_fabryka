<?php

declare(strict_types=1);

namespace Tests\Fake;

use Activities\Usage\ActivityRepositoryInterface;
use Activities\ActivityInterface;

final class ActivityRepository implements ActivityRepositoryInterface
{
    /**
     * @var ActivityInterface[]
     */
    private array $activities = [];

    public function save(ActivityInterface $activity): void
    {
        $this->activities[$activity->getTopic()] = $activity;
    }

    public function findOneByTopic(string $topic): ?ActivityInterface
    {
        foreach ($this->activities as $activity) {
            if ($topic === $activity->getTopic()) {
                return $activity;
            }
        }

        return null;
    }
}
